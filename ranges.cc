#include <iostream>
#include <opencv2/opencv.hpp>
#define CTL_WIN_NAME "CONTROL"
using namespace std;
using namespace cv;

Mat frame;
int minh = 10, mins = 100, minv = 100, maxh = 40, maxs=255, maxv=255;
int minr = 200, ming = 200, minb = 200, maxr = 255, maxg=255, maxb=255;
int alpha=100, beta=100, _gamma = 100;

int main()
{
    VideoCapture cam;
    if (!cam.open(-1))
        return -1;
    namedWindow( CTL_WIN_NAME);
    moveWindow(CTL_WIN_NAME, 0,10);

    createTrackbar( "Min H", CTL_WIN_NAME, &minh, 255);
    createTrackbar( "Min S", CTL_WIN_NAME, &mins, 255);
    createTrackbar( "Min V", CTL_WIN_NAME, &minv, 255);
    createTrackbar( "Max H", CTL_WIN_NAME, &maxh, 255);
    createTrackbar( "Max S", CTL_WIN_NAME, &maxs, 255);
    createTrackbar( "Max V", CTL_WIN_NAME, &maxv, 255);

    createTrackbar( "Min B", CTL_WIN_NAME, &minb, 255);
    createTrackbar( "Min G", CTL_WIN_NAME, &ming, 255);
    createTrackbar( "Min R", CTL_WIN_NAME, &minr, 255);
    createTrackbar( "Max B", CTL_WIN_NAME, &maxb, 255);
    createTrackbar( "Max G", CTL_WIN_NAME, &maxg, 255);
    createTrackbar( "Max R", CTL_WIN_NAME, &maxr, 255);

    createTrackbar( "alpha", CTL_WIN_NAME, &alpha, 100);
    createTrackbar( "beta", CTL_WIN_NAME, &beta, 100);
    createTrackbar( "gamma", CTL_WIN_NAME, &_gamma, 200);

    while(true) {
        Mat hsv, r, y, l, d, x, w, j;
        cam >> frame;

        Ptr<CLAHE> clahe = createCLAHE(2.);
        vector<Mat> channels;
        split(frame, channels);
        for (auto& c : channels) {
            clahe->apply(c, c);
        }
        merge(channels, frame);
        
        cvtColor(frame, hsv, COLOR_BGR2HSV);
        if (frame.empty())
            return -2;

        inRange(frame, Scalar(minb, ming, minr), Scalar(maxb, maxg, maxr), x);
        bitwise_and(frame, frame, w, x);
        
        inRange(hsv, Scalar(minh, mins, minv), Scalar(maxh, maxs, maxv), y);
        bitwise_and(hsv, hsv, j, y);

        addWeighted(w, alpha / 100., j, beta / 100., (_gamma - 100)/10., r);
        imshow("img", r);
        int c;
        c = waitKey( 20 );
        if( (char)c == 27 )
            break;
    }

    cout << " minh=" <<  minh << endl;
    cout << " mins=" <<  mins << endl;
    cout << " minv=" <<  minv << endl;
    cout << " maxh=" <<  maxh << endl;
    cout << " maxs=" <<  maxs << endl;
    cout << " maxv=" <<  maxv << endl;

    cout << " minr=" <<  minr << endl;
    cout << " ming=" <<  ming << endl;
    cout << " minb=" <<  minb << endl;
    cout << " maxr=" <<  maxr << endl;
    cout << " maxg=" <<  maxg << endl;
    cout << " maxb=" <<  maxb << endl;

    cout << " alpha=" <<  alpha << endl;
    cout << " beta=" <<  beta << endl;
    cout << " _gamma=" <<  _gamma << endl;


    return 0;
}
