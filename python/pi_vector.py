#!/usr/bin/python3

from math import sqrt, pi, sin, cos


class Vector:
	''' Immutable 2D vector class '''
	def __init__(self, x, y):
		self._x = float(x)
		self._y = float(y)
		self._size = sqrt(self._x ** 2. + self._y ** 2.)

	@property
	def x(self):
		return self._x

	@property
	def y(self):
		return self._y

	@property
	def size(self):
		return self._size

	def normal(self):
		return Vector(self._x / self._size, self._y / self._size) if self._size > 0 else self

	def rotate(self, angle):
		angle *= (pi / 180.)
		s, c = sin(angle), cos(angle)
		return Vector(c * self._x - s * self._y, s * self._x + c * self._y)

	def __str__(self):
		return "({:.4}, {:.4})".format(self._x, self._y)		

	def __neg__(self):
		return Vector(-self._x, -self._y)

	def __add__(self, other):
		if other is None:
			return self
		else: 
			return Vector(self._x + other._x, self._y + other._y) 			

	def __sub__(self, other):
		return self + (-other)

	def __mul__(self, s):
		return Vector(self._x * s, self._y * s)

	def __rmul__(self, s):
		return self.__mul__(s)

	def __imul__(self, s):
		return self.__mul__(s)

	def __iadd__(self, other):
		return self.__add__(other)

	def __isub__(self, other):
		return self.__sub__(other)
