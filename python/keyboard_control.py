#!/usr/bin/python3

import tkinter as tk
import os
from pi_vector import Vector
from getpass import getuser

# import mock class for tests in the computer  
isRpi = getuser() == "pi"
if isRpi: 
	from controller import Controller 
else:
	from rpi_mock import Controller
 

class MainWindow(tk.Tk):
	''' Handles the keyboard events and calculates the direction '''
	def __init__(self):
		tk.Tk.__init__(self)
		#Configure key bindings and data structures
		self.direction = Vector(0, 0)
		self.controller = Controller()
		self.bind("<Escape>", lambda _ : self.quit()) 
		self.bind("<FocusOut>", self.clear_direction)
		#self.bind("<KeyPress-Space>", lambda _ : self.controller.horn(True))
		#self.bind("<KeyRelease-Space>", lambda _ : self.controller.horn(False))
		self.keys = {"Up" : Vector(0, 1), 
	 				 "Down" : Vector(0, -1), 
	 				 "Right" : Vector(1, 0), 
	 				 "Left" : Vector(-1, 0)}
		for n in self.keys:
			self.bind("<KeyPress-" + n + ">", self.keydown)
			self.bind("<KeyRelease-" + n + ">", self.keyup)
		# Build window
		self.title("Raspberry pi autonomous car")
		self.width = 300
		self.height = 200
		self.label_height = 40
		self.geometry("{}x{}".format(self.width, self.height))
		self.columnconfigure(0, minsize=self.width)
		self.rowconfigure(0, minsize=self.height - self.label_height)
		self.rowconfigure(1, minsize=self.label_height)
		#Build widgets
		self.label = tk.Label(self, text="Use the direction keys in your\nkeyboard to move the car", 
			font=("Helvetica", 10), anchor=tk.CENTER);
		self.shape = None
		self.canvas = tk.Canvas(self, width=self.width, height=self.height - self.label_height)
		center_x, center_y = self.width / 2., (self.height - self.label_height) / 2.
		self.canvas.create_oval(center_x - 10, center_y - 10, center_x + 10, center_y + 10, fill="red")
		# Draw the widgets
		self.canvas.grid(column = 0, row=0, sticky=tk.N+tk.S+tk.E+tk.W)
		self.label.grid(column=0, row=1, sticky=tk.N+tk.S+tk.E+tk.W)
		self.after(100, self.check_collision)
	
	def clear_direction(self, e):
		self.direction = Vector(0, 0)
		self.update()
	
	def update(self):
		def translate(p, v): # when drawing in the GUI the y axis is inverted
			return Vector(p.x + v.x, p.y - v.y)

		if self.shape is not None:
			self.canvas.delete(self.shape)
		
		center = Vector(self.width / 2., (self.height - self.label_height) / 2.)
		d = translate(center, self.direction.normal() * 50) 
		self.shape = self.canvas.create_line(center.x, center.y, d.x, d.y, width=10, fill="red")
		#sends the direction vector to the controller 
		#Equivalent to self.controller.go_to(self.direction)
		self.controller << self.direction

	def keydown(self, e):
		self.direction += self.keys[e.keysym]
		self.update()

	def keyup(self, e):
		self.direction -= self.keys[e.keysym]
		self.update()

	def check_collision(self):
		pass
		self.controller.check_collision()
		self.after(100, self.check_collision)

def main(): 
	os.system("xset r off")
	try:
		win = MainWindow()
		win.focus_set()
		win.mainloop()
		os.system("xset r on")
	except:
		#p.terminate()
		os.system("xset r on")

if __name__ == '__main__':
	main()