#!/usr/bin/python3

import RPi.GPIO as GPIO
from time import sleep

BACK_MOTOR_DATA_ONE = 17
BACK_MOTOR_DATA_TWO = 27
BACK_MOTOR_ENABLE_PIN = 18
FRONT_MOTOR_DATA_ONE = 19
FRONT_MOTOR_DATA_TWO = 26
INITIAL_PWM_DUTY_CYCLE = 100
PWM_FREQUENCY = 1000
COLLISION_PIN = 2
HORN_FREQUENCY = 500

class Controller:
	
	def __init__(self):
		self.collided = False
		GPIO.setmode(GPIO.BCM)
		GPIO.setup(BACK_MOTOR_DATA_ONE, GPIO.OUT)
		GPIO.setup(BACK_MOTOR_DATA_TWO, GPIO.OUT)
		GPIO.setup(FRONT_MOTOR_DATA_ONE, GPIO.OUT)
		GPIO.setup(FRONT_MOTOR_DATA_TWO, GPIO.OUT)
		GPIO.setup(COLLISION_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
		GPIO.setup(BACK_MOTOR_ENABLE_PIN, GPIO.OUT)
		self._pwm = GPIO.PWM(BACK_MOTOR_ENABLE_PIN, PWM_FREQUENCY)
		self._pwm.start(INITIAL_PWM_DUTY_CYCLE)
		# self.pwm.start(INITIAL_PWM_DUTY_CYCLE)
		# GPIO.output(BACK_MOTOR_ENABLE_PIN, True)

	def __lshift__(self, dir):
		''' Yes, very much like C++ '''
		self.go_to(dir)

	def go_to(self, dir):
		if dir.size > 0:
			if dir.y == 0:
				self.back_idle()
			elif dir.x == 0:
				self.front_idle()

			if dir.y > 0:
				self.forward()
			elif dir.y < 0:
				self.reverse()

			if dir.x > 0:
				self.right()
			elif dir.x < 0:
				self.left()
		else:
			self.stop()		

	def forward(self):
		if not self.collided:
			GPIO.output(BACK_MOTOR_DATA_ONE, True)
			GPIO.output(BACK_MOTOR_DATA_TWO, False)

	def reverse(self):
		GPIO.output(BACK_MOTOR_DATA_ONE, False)
		GPIO.output(BACK_MOTOR_DATA_TWO, True)

	def left(self):
		GPIO.output(FRONT_MOTOR_DATA_ONE, True)
		GPIO.output(FRONT_MOTOR_DATA_TWO, False)

	def right(self):
		GPIO.output(FRONT_MOTOR_DATA_ONE, False)
		GPIO.output(FRONT_MOTOR_DATA_TWO, True)

	def stop(self):
		# self.reverse()
		# sleep(0.2)
		self.back_idle()
		self.front_idle()

	def front_idle(self):
		GPIO.output(FRONT_MOTOR_DATA_ONE, False)
		GPIO.output(FRONT_MOTOR_DATA_TWO, False)

	def back_idle(self):
		GPIO.output(BACK_MOTOR_DATA_ONE, False)
		GPIO.output(BACK_MOTOR_DATA_TWO, False)

	def horn(self, active):
		pass

	def check_collision(self):
		self.collided = not GPIO.input(COLLISION_PIN)
		if self.collided:
			GPIO.output(BACK_MOTOR_DATA_ONE, False)