#!/usr/bin/python3


class Controller:
	
	def __init__(self):
		pass

	def __lshift__(self, dir):
		''' Yes, very much like C++ '''
		if dir.size > 0:
			if dir.y == 0:
				self.back_idle()
			elif dir.x == 0:
				self.front_idle()

			if dir.y > 0:
				self.forward()
			elif dir.y < 0:
				self.reverse()

			if dir.x > 0:
				self.right()
			elif dir.x < 0:
				self.left()
		else:
			self.stop()
		

	def forward(self):
		print("forward")

	def reverse(self):
		print("reverse")

	def left(self):
		print("left")

	def right(self):
	    print("right")

	def stop(self):
		print("stop")

	def front_idle(self):
		pass

	def back_idle(self):
		pass

	def horn(self, active):
		pass

	def check_collision(self):
		pass
