#include "LineDetector.h"

using namespace std;
using namespace cv;
using namespace Rpicar;

void detect(CV_IN_OUT frame_t& frame);
Vec4d isolate(CV_IN_OUT frame_t& frame);

const string keys =
        "{interactive i|            | Show images in interactive dialog whilst processing}"
        "{fps f        | 10         | Fps captured and writen to the file or shown in screen}"
        "{frameCount c | 100        | Number of frames captured before stopping. Implies non-interactive}"
        "{output o     | output.avi | output file. Implies non-interactive}"
        "{help h ?     |            |}";

int main(int argc, const char** argv)
{
    CommandLineParser parser(argc, argv, keys);
    parser.about("Imgdetection v1.0");
    if (parser.has("help")) {
        parser.printMessage();
        return 0;
    }

    bool interactive = parser.has("interactive");
    auto frameCount = parser.get<int>("frameCount");
    auto output = parser.get<string>("output");
    auto fps = parser.get<int>("fps");
    if ((uint)fps > 10) {
        piwrn<Capture>() << "Invalid fps value (" << fps << "). Setting to 10 fps." << endl;
        fps = 10;
    }

    if (!parser.check())
    {
        parser.printErrors();
        return 0;
    }

    VideoWriter writer;
    Mat frame;
    cam() >> frame;
    if (!interactive)
        if (!writer.open(output, CV_FOURCC_MACRO('M', 'P', 'E', 'G'), fps, frame.size())) {
            pierr<Debug>() << "cannot open file:" <<  output << endl;
        }
    LineDetector detector;

    int i = 0, m = 10 / fps;
    while ((cam() >> frame) && (interactive || (i < frameCount))) {
        if (i % m == 1) continue;
        Mat x = Mat::zeros(frame.size(), frame.type());
        detector(frame, x);
        pitrc<LineDetector>() << "Frames processed: " << i << endl;

        if (!interactive) {
            writer << x;
            ++i;
            continue;
        }

        imshow("", x);
        int c = waitKey(10);
        if((char)c == 27) break;
    }
    SysController().stop();
    return 0;
}

void detect(CV_IN_OUT frame_t& frame) {

}

Vec4d isolate(CV_IN_OUT frame_t& frame) {
    Mat hsvImg;
    Mat roi;
    Mat out;
    cvtColor(frame, hsvImg, COLOR_RGB2GRAY);
    inRange(hsvImg, Scalar(150, 150, 150), Scalar(255, 255, 255), out);
    GaussianBlur(out, hsvImg, Size(21, 21), 4);
    Canny(hsvImg, out, 100, 150, 3, true);
    vector<Vec4i> lines;
    HoughLinesP(out, lines, .6, M_PI / 180.0, 90, 30, 30 );
    int h = frame.size().height - 20;
    Vec4d direction;
    for (auto& l : lines) {
        if (l[1] >= h || l[3] >= h) {
            for (int i = 0; i < 4; ++i)
                direction[i] = direction[i] ? (l[i] + direction[i]) / 2. : (double)l[i];
        }
    }
    line(frame, Point(direction[0], direction[1]), Point(direction[3], direction[4]), Scalar(0, 0, 255), 8);
    return direction;
}
