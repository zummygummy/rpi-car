
#include <wiringPi.h>
#include <unistd.h>
#include "RpicarUtil.h"

#ifndef IMGDETECTION_CONTROLER_H
#define IMGDETECTION_CONTROLER_H

#define BACK_MOTOR_DATA_ONE 17
#define BACK_MOTOR_DATA_TWO 27
#define BACK_MOTOR_ENABLE_PIN 18
#define FRONT_MOTOR_DATA_ONE 19
#define FRONT_MOTOR_DATA_TWO 26
#define MAX_PWM_FREQUENCY 1000

namespace Rpicar {

    // Pseudo-immutable Vector
    struct Vector {
    public:
        Vector(double x, double y);
        explicit Vector(const Vec4i& vec): Vector(vec[2]-vec[0], vec[3]-vec[1]) {};
        Vector(const Vector& vec): Vector(vec.x(), vec.y()) {};
        Vector(): Vector(0., 0.) {}
        inline double x() const { return this->_x; }
        inline double y() const { return this->_y; }
        double length() const { return this->_length; }
        inline double slope() const { return this->_slope; }
        inline Vector &absolute() const { return *new Vector(abs(this->_x), abs(this->_y)); }

        Vector &rotate(double angle) const;
        Vector &normal() const;
        Vector &operator-() const;
        Vector &operator+(const Vector& other) const;
        Vector &operator*(double scalar) const;
        void operator+=(const Vector& other);
        void operator*=(double scalar);

    private:
        double _x;
        double _y;
        double _length;
        double _slope;
    };

    inline ostream& operator<<(ostream& stream, const Vector &v) {
        stream << "(" << v.x() << ", " << v.y() << ")";
        return stream;
    }

    enum Motor : int {
        BACK = 1, FRONT, BOTH
    };

    class _Controller {
    public:
        struct Wait {
            friend class _Controller;
            explicit Wait(uint milliseconds) : milliseconds(milliseconds) {}
        private:
            uint milliseconds;
        };
        _Controller &right();
        _Controller &left();
        _Controller &forward(int velocity);
        _Controller &reverse(int velocity);
        _Controller &stop();
        _Controller &idle(Motor which);
        _Controller &after(uint milliseconds);
        _Controller &operator<<(const Vector& dirVec);
        _Controller &operator<<(const Wait& wait);
        static inline _Controller &instance() { return *(controller ? controller : controller = new _Controller()); }
    protected:
        _Controller();
        static _Controller* controller;
    };

    using Wait = _Controller::Wait;

    class NoController {
    public:
        NoController &right() {}
        NoController &left() {}
        NoController &forward(int velocity) {}
        NoController &reverse(int velocity) {}
        NoController &stop() {}
        NoController &idle(Motor which) {}
        NoController &after(uint milliseconds) {}
        NoController &operator<<(const Vector& dirVec) {}
        NoController &operator<<(const Wait& wait) {}
        static inline NoController &instance() { return *(controller ? controller : controller = new NoController()); }
    private:
        static NoController *controller;
    };
    using Controller = conditional<Traits<Build>::useCVCam, NoController, _Controller>::type;

    inline Controller &SysController() {
        return Controller::instance();
    }

}

#endif //IMGDETECTION_CONTROLER_H
