
#include <iostream>
#include <ctgmath>

#ifndef IMGDETECTION_TRAITS_H
#define IMGDETECTION_TRAITS_H
namespace Rpicar {
    using namespace std;

    class Build {};
    class Time {};
    class Debug;
    class LineDetector;



#define TRAIT static constexpr
    template<typename T>
    struct Traits {
        TRAIT bool isEnabled = true;
        TRAIT bool isDebugged = true;
    };

    template<>
    struct Traits<Build> : public Traits<void> {
        TRAIT uint cores = 4;
        TRAIT bool useCVCam = true;
    };
    template<>
    struct Traits<Time> : public Traits<void> {
        enum {
            MICRO_SEC, MILLI_SEC, SEC
        };
        TRAIT uint measureUnit = MILLI_SEC;
    };
    template<>
    struct Traits<Debug> : public Traits<void> {
        TRAIT bool showError = true;
        TRAIT bool showWarning = true;
        TRAIT bool showTrace = false;
    };
}
#endif //IMGDETECTION_TRAITS_H
