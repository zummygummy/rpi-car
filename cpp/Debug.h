

#include <cxxabi.h>
#include "Traits.h"
#include <type_traits>
#ifndef IMGDETECTION_DEBUG_H
#define IMGDETECTION_DEBUG_H
namespace Rpicar {
    class Debug {
    public:
        template<typename T>
        Debug &operator<<(T val) {
            cout << val;
            return *this;
        }
        Debug &operator<<(ostream& (*pf)(ostream&)) {
            cout << pf << "\033[0m";
            return *this;
        }
    };

    class NoDebug {
    public:
        template<typename T>
        NoDebug &operator<<(T val) { return *this; }
        NoDebug &operator<<(ostream& (*pf)(ostream&)) { return *this; }
    };

    template<bool b, typename T>
    using Debugger = typename conditional<b && Traits<T>::isDebugged, Debug, NoDebug>::type;

    template <class T>
    inline char* demangled() {
        return abi::__cxa_demangle(typeid(T).name(), nullptr, nullptr, nullptr);
    }
    template <bool b, class T>
    inline Debugger<b, T> __default_dbg(const char* text) {
        return Debugger<b, T>() << text << "[" << demangled<T>() << "]: ";
    }


    template <typename T> inline Debugger<Traits<Debug>::showError, T>
    pierr() {
        return __default_dbg<Traits<Debug>::showError, T>("\033[31m\033[1mError\033[21;31m");

    }

    template <typename T> inline Debugger<Traits<Debug>::showWarning, T>
    piwrn() {
        return __default_dbg<Traits<Debug>::showWarning, T>("\033[33m\033[1mWarning\033[21;33m");
    }

    template <typename T> inline Debugger<Traits<Debug>::showTrace, T>
    pitrc() {
        return __default_dbg<Traits<Debug>::showTrace, T>("\033[33m\033[1mWarning\033[21;33m");
    }
}
#endif //IMGDETECTION_DEBUG_H
