
#include "_Controller.h"
namespace Rpicar {
    _Controller* _Controller::controller = nullptr;
    NoController* NoController::controller = nullptr;

    _Controller::_Controller() {
        wiringPiSetupGpio();
        pinMode(BACK_MOTOR_DATA_ONE, OUTPUT);
        pinMode(BACK_MOTOR_DATA_TWO, OUTPUT);
        pinMode(FRONT_MOTOR_DATA_ONE, OUTPUT);
        pinMode(FRONT_MOTOR_DATA_TWO, OUTPUT);
//        pwmSetMode(PWM_MODE_BAL);
//        pinMode(BACK_MOTOR_ENABLE_PIN, PWM_OUTPUT);
    }

    _Controller &_Controller::forward(int velocity) {
//        assert((uint) velocity <= MAX_PWM_FREQUENCY);
        digitalWrite(BACK_MOTOR_DATA_ONE, HIGH);
        digitalWrite(BACK_MOTOR_DATA_TWO, LOW);
        //pwmWrite(BACK_MOTOR_ENABLE_PIN, velocity);
//        pwmSetClock(velocity);
        return *this;
    }

    _Controller &_Controller::right() {
        digitalWrite(FRONT_MOTOR_DATA_ONE, LOW);
        digitalWrite(FRONT_MOTOR_DATA_TWO, HIGH);
        return *this;
    }

    _Controller &_Controller::left() {
        digitalWrite(FRONT_MOTOR_DATA_ONE, HIGH);
        digitalWrite(FRONT_MOTOR_DATA_TWO, LOW);
        return *this;
    }

    _Controller &_Controller::reverse(int velocity) {
        assert((uint) velocity <= MAX_PWM_FREQUENCY);
        digitalWrite(BACK_MOTOR_DATA_ONE, LOW);
        digitalWrite(BACK_MOTOR_DATA_TWO, HIGH);
        pwmWrite(BACK_MOTOR_ENABLE_PIN, velocity);
        return *this;
    }

    _Controller& _Controller::stop() {
        return this->idle(Motor::BOTH);
    }

    _Controller& _Controller::after(uint milliseconds) {
        usleep(milliseconds * 1000);
        return *this;
    }

    _Controller &_Controller::idle(Motor which) {
        int modeBack = ((int)which) & 1, modeFront = (((int)which) >> 1) & 1;
        digitalWrite(BACK_MOTOR_DATA_ONE, modeBack);
        digitalWrite(BACK_MOTOR_DATA_TWO, modeBack);
        digitalWrite(FRONT_MOTOR_DATA_ONE, modeFront);
        digitalWrite(FRONT_MOTOR_DATA_TWO, modeFront);
        return *this;
    }

    _Controller &_Controller::operator<<(const Vector &direction) {
        int velocity = MAX_PWM_FREQUENCY;//(int) floor(direction.length());

        if (direction.length() == 0) {
            this->stop();
            return *this;
        }
        // Y axis
        if (direction.y() == 0)
            this->idle(BACK);
        else if (direction.y() > 0)
            this->forward(velocity);
        else if (direction.y() < 0)
            this->reverse(velocity);

        // X axis
        if (direction.x() == 0)
            this->idle(FRONT);
        else if (direction.x() > 0)
            this->right();
        else if (direction.x() < 0)
            this->left();

        return *this;
    }
    _Controller& _Controller::operator<<(const Wait &wait) {
        this->after(wait.milliseconds);
        return *this;
    }

    Vector::Vector(double x, double y) {
        this->_x = x;
        this->_y = y;
        this->_length = sqrt(pow(this->_x, 2) + pow(this->_y, 2));
        this->_slope = x ? y / x : 0.;
    }

    Vector &Vector::rotate(double angle) const {
        angle *= (M_PI/180.0);
        double s = sin(angle), c = cos(angle);
        return *(new Vector(c * this->_x - s * this->_y, s * this->_x + c * this->_y));
    }

    Vector &Vector::normal() const {
        return *(new Vector(this->_x / this->_length, this->_y / this->_length));
    }

    Vector &Vector::operator-() const {
        return *(new Vector(-this->_x, -this->_y));
    }

    Vector &Vector::operator+(const Vector &other) const {
        return *(new Vector(this->_x + other._x, this->_y + other._y));
    }

    Vector &Vector::operator*(double scalar) const {
        return *(new Vector(scalar * this->_x, scalar * this->_y));
    }

    void Vector::operator+=(const Vector &other) {
        this->_x += other._x;
        this->_y += other._y;
    }

    void Vector::operator*=(double scalar) {
        this->_x *= scalar;
        this->_y *= scalar;

    }
}
