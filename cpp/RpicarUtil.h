
#include <thread>
#include <queue>
#include <utility>
#include <atomic>
#include <mutex>
#include <cmath>
#include <functional>
#include <opencv/cv.hpp>
#include <raspicam/raspicam_cv.h>
#include "Debug.h"

#ifndef IMGDETECTION_RPICARUTIL_H
#define IMGDETECTION_RPICARUTIL_H


namespace Rpicar {
    using namespace raspicam;
    using namespace cv;

    typedef Mat frame_t;
    typedef add_pointer<frame_t>::type frame_ptr;
    using frame_ref = const frame_t &;

    struct Capture {};
    template<>
    struct Traits<Capture> : public Traits<void> {
        TRAIT int frameWidth = 640;
        TRAIT int frameHeight = 480;
    };

    template <bool> class Camera {
    public:
        Camera() {
            this->cam = new VideoCapture();
            this->cam->set(CV_CAP_PROP_FRAME_WIDTH, Traits<Capture>::frameWidth);
            this->cam->set(CV_CAP_PROP_FRAME_HEIGHT, Traits<Capture>::frameHeight);

            if (!this->cam->open(-1))
                pierr<Debug>() << "Could not open camera with opencv"  << endl;
        }
        inline bool operator>>(CV_OUT frame_t& frame) {
            *(cam) >> frame;
            return !frame.empty();
        }
        inline double get(int propID) { return this->cam->get(propID); }
    private:
        VideoCapture *cam;
    };


    template<>
    class Camera<false> {
    public:

        Camera() {
            this->cam = new RaspiCam_Cv();
            this->cam->set(CV_CAP_PROP_FRAME_WIDTH, Traits<Capture>::frameWidth);
            this->cam->set(CV_CAP_PROP_FRAME_HEIGHT, Traits<Capture>::frameHeight);
            this->cam->set(CV_CAP_PROP_BRIGHTNESS, 50);
            this->cam->set(CV_CAP_PROP_CONTRAST, 60);
            if (!this->cam->open())
                pierr<Debug>() << "Could not open camera with rapicam" << endl;
        }

        inline bool operator>>(CV_IN_OUT frame_t& frame) {
            cam->grab();
            cam->retrieve(frame);
            return !frame.empty();
        }

        inline double get(int propID) { return this->cam->get(propID); }

    private:
        RaspiCam_Cv *cam;
    };

    typedef Camera<Traits<Build>::useCVCam> Cam;

    inline Cam &cam() {
        static Cam *cam;
        return *(cam ? cam : cam = new Cam());
    }



    #define MakeChrono(name) Chrono<Traits<Time>::isDebugged> name
    template <bool>
    struct Chrono {
        int64 start;
        static constexpr const char* names[3] = { "mis", "ms", "s"};
        static constexpr int mult[3] = {1000000, 1000, 1};

    public:
        Chrono(){
            this->start = getTickCount();
        }

        inline void operator() (InputOutputArray frame) {
            int64 now = getTickCount();
            double val = (now - this->start) / ( getTickFrequency() / mult[Traits<Time>::measureUnit]);
            putText(frame, format("%.4f %s", val, names[Traits<Time>::measureUnit]), Point(20, 20), FONT_HERSHEY_SIMPLEX, 0.8, Scalar(0,255,0), 2, LINE_AA);
        }

        inline void operator() () {
            int64 now = getTickCount();
            double val = (now - this->start)/( getTickFrequency()/ mult[Traits<Time>::measureUnit]);
            pitrc<Time>() << val << " " << names[Traits<Time>::measureUnit] << endl;
        }
    };

    template <>
    struct Chrono<false> {
    public:
        inline void operator() (InputOutputArray frame) {  }
        inline void operator() () { }
    };
}
#endif //IMGDETECTION_RPICARUTIL_H
