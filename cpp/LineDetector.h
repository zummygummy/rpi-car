#include <ctime>
#include <vector>
#include <array>

#include "_Controller.h"

#ifndef IMGDETECTION_LINEDETECTOR_H
#define IMGDETECTION_LINEDETECTOR_H

namespace Rpicar {

    class LineDetector {
    public:
        using Settings = Traits<LineDetector>;
        LineDetector();
        void operator()(frame_ref src, frame_ref dest);

    private:
        void mkROIMask(frame_t& dest);
        void detectLine(frame_ref src, frame_ref dest);
        frame_t roiMask;


    };

    template<>
    struct Traits<LineDetector> : public Traits<void> {
        TRAIT bool isDebugged = false;
        // Gaussian blur
        TRAIT int kernelSize = 3;
        // Canny
        TRAIT int lowThreshold = 50;
        TRAIT int highThreshold = 150;
        // ROI (trapezoid)
        // Dimensions are expressed as percentage of image width/height
        TRAIT double roiBottomWidth = .85;
        TRAIT double roiTopWidth = .7;
        TRAIT double roiHeight = .4;
        // Hough line transform
        TRAIT int rho = 2; // pixels
        TRAIT double theta = M_PI / 180.; // radians
        TRAIT int houghThreshold = 60;
        TRAIT int minLineLenght = 90; // pixels
        TRAIT int maxLineGap = 40;  // pixels
        // The number of frames that are processed
        // before changing direction
        TRAIT uint frameAcc = 3;
    };

}
#endif //IMGDETECTION_LINEDETECTOR_H
