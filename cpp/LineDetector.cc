
#include <cstdarg>
#include "LineDetector.h"

namespace Rpicar {

    LineDetector::LineDetector() {
        this->mkROIMask(this->roiMask);
    }

    void LineDetector::operator()(frame_ref src, frame_ref dest) {
        MakeChrono(cr);
        Mat x;
        src.copyTo(x);
        this->detectLine(x, dest);
        cr(dest);
    }

    void LineDetector::detectLine(frame_ref src, frame_ref dest) {
        GaussianBlur(src, src, Size(Settings::kernelSize, Settings::kernelSize), 4);
        Ptr<CLAHE> clahe = createCLAHE(2.);
        vector<Mat> channels;
        split(src, channels);

        for (auto &c : channels) {
            clahe->apply(c, c);
        }
        merge(channels, src);
        Mat gray(src.size(), CV_8UC1);
        Mat img(src.size(), CV_8UC1);
        cvtColor(src, img, COLOR_BGR2GRAY);
        Mat mask(src.size(), CV_8UC1);
        Canny(img, gray, Settings::lowThreshold, Settings::highThreshold, 3, true);
        bitwise_and(gray, this->roiMask, mask);
        vector<Vec4i> lines;
        HoughLinesP(mask, lines, Settings::rho, Settings::theta, Settings::houghThreshold, Settings::minLineLenght,
                    Settings::maxLineGap);
        static Vector direction;
        static int frameCount;
        int c, r, l;
        c = r = l = 0;
        for (auto &v : lines) {
            Vector vc(v);
            if (abs(vc.slope()) > .2) {
                if (vc.slope() > 0) r++;
                if (vc.slope() < 0) l++;
                direction += vc;
                ++c;
                line(dest, Point(v[0], v[1]), Point(v[2], v[3]), Scalar(255, 0, 0), 3);
            }
        }

        if (c) {
            direction *= (1. / (double) c);
            Point s(dest.size().width >> 1, dest.size().height);
            line(dest, s, Point(s.x + direction.x(), s.y + direction.y()), Scalar(0, 255, 0), 3);
        }
        if ((++frameCount) == 5) {
            pitrc<Controller>() << direction << endl;
            SysController() << direction.absolute();
            direction *= 0;
            frameCount = 0;
        }
    }

    void LineDetector::mkROIMask(frame_t &dest) {
        Size sz(Traits<Capture>::frameWidth, Traits<Capture>::frameHeight);
        dest = Mat::zeros(sz, CV_8UC1);

        int h = sz.height, w = sz.width;
        int topWidth = cvFloor(w * Settings::roiTopWidth),
                bottomWidth = cvFloor(w * Settings::roiBottomWidth),
                height = cvFloor(h * Settings::roiHeight);
        int yTop = (h - height),
                gapTop = (w - topWidth) >> 1,
                gapBottom = (w - bottomWidth) >> 1;
        vector<Point> points({Point(gapBottom, h), Point(w - gapBottom, h),
                              Point(w - gapTop, yTop), Point(gapTop, yTop)});
        fillConvexPoly(dest, points, Scalar(255));
    }
}
